# How to write a rule

The rules to verify are stored in a text file, one by one.
 
A rule contains the following ordered-elements: 


* *rule_number*: number
* *type*: word, either `BANDWIDTH_ANALYSIS` or `PROFILE_ANALYSIS`
* *verify*: word, either `PROTOCOL` or `CATEGORY`, indicating whether verifying on the categories or the protocol/applications
* *description*: string, description of the rule, no space is allowed
* *user_ip*: word, either  `*`, or `e` or an IP address
* *protocol*: word, either `*`, or `e` or a protocol name or a category name (depending on `verify`)
* *current_period*: number,
* *relative_hour_before*: number,
* *relative_day_before*: number,
* *relative_week_before*: number,
* *relative_month_before*: number,
* *relative_year_before*: number,
* *number_of_groups_to_average*: number,
* *working_days_only*: number,
* *threashold* : number, defined a threshold bypass we can conclude a change

## Example

```
rule_number = 1
type = BANDWIDTH_ANALYSIS
verify = PROTOCOL
description = Compare_bandwidth_use_of_last_hour_with_average_of_the_same_hour_in_the_last_3_weeks
user_ip = *
protocol = e
current_period = 1
relative_hour_before = 0
relative_day_before = 0
relative_week_before = 0
relative_month_before = 0
relative_year_before = 0
number_of_groups_to_average = 1
working_days_only = 0
threashold = 10
```

## Mask of User_IP or Protocol name

### `BANDWIDTH_ANALYSIS`

A bandwidth-analysis rule verifies a change bandwidth of IP, or application, or the overall network, etc.
This depends on pair of properties (`rule->user_ip`, `rule->protocol_name`):
 
- (`*`, `*`)  : bandwidth of the overall network
- (`e`, `*`)  : bandwidth of each ip
- (`*`, `e`)  : bandwidth of each protocol/application
- (`e`, `e`)  : bandwidth of each ip of each protocol
- (`xxx`, `*`): bandwidth of ip = `xxx`
- (`*`, `yyy`): bandwidth of proto = `yyy`
- (`xxx`, `e`): bandwidth of each proto of ip = `xxx`
- (`e`, `yyy`): bandwidth of each ip of proto = `yyy`
- (`xxx`, `yyy`): bandwidth of ip = `xxx` and proto = `yyy`
    
The applications is replaced by categories if the rule contains `verify = CATEGORY`

### `BEHAVIOUR_ANALYSIS`

A behaviour-analysis rule verifies a change of behaviour of the overall network, or each IP in the network, or one specific IP depending on `user_ip` property.
The `user_ip` property can receive one of the following:

- `*`  : change category of the overall network
- `e`  : change category of each ip
- `xxx`: change category of ip = `xxx`

The behaviour is either category or application, depending on `verify` property of the rule.
