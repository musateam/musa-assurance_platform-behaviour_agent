# MMT Data Format

Redis channel name: `behaviour.report`

Format id: 

- 11: bandwidth analysis
- 12: profile analysis

## Bandwidth analysis report

| # | Column Name   | Column Description | 
| - | ------------- | ------------------ | 
| 1 | *format*      | number: 11 | 
| 2 | *probe*       | number: Identifier of theprobe generating the report. | 
| 3 | *source*      | string: Identifier of the data source whether it is a trace file name or a network interface. | 
| 4 | *timestamp*   | number: Timestamp (seconds.micros) corresponding to the time when the output row was reported. | 
| 5 | *property_id* | number: identifying the property |
| 6 | *IP*          | string: IP address |
| 7 | *app*         | string: protocol/app name |
| 8 | *before*      | number:  bandwidth before|
| 9 | *after*       | number: bandwidth after |
| 10| *verdict*     | word: `INCONCLUSIVE`, `CHANGE_CATEGORY`, `NO_CHANGE_CATEGORY`, `CHANGE_BANDWIDTH`, `NO_CHANGE_BANDWIDTH`, `NO_ACTIVITY_BEFORE`, `NO_ACTIVITY_CURRENT`
| 11| *description* | string: explication of the property |

## Behaviour analysis report

| # | Column Name   | Column Description | 
| - | ------------- | ------------------ | 
| 1 | *format*      | number: 12 | 
| 2 | *probe*       | number: Identifier of theprobe generating the report. | 
| 3 | *source*      | string: Identifier of the data source whether it is a trace file name or a network interface. | 
| 4 | *timestamp*   | number: Timestamp (seconds.micros) corresponding to the time when the output row was reported. | 
| 5 | *property_id* | number: identifying the property |
| 6 | *IP*          | string: IP address |
| 7 | *before*      | string: app/category before|
| 8 | *after*       | string: app/category after |
| 9 | *ba-before*   | number:  bandwidth before|
| 10| *ba-after*    | number: bandwidth after |
| 11| *verdict*     | word: `INCONCLUSIVE`, `CHANGE_CATEGORY`, `NO_CHANGE_CATEGORY`, `CHANGE_BANDWIDTH`, `NO_CHANGE_BANDWIDTH`, `NO_ACTIVITY_BEFORE`, `NO_ACTIVITY_CURRENT`
| 12| *description* | string: explication of the property |