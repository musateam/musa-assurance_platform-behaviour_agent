var mmtAdaptor      = require('./libs/dataAdaptor');
var dbc             = require('./libs/mongo_connector');
var probe           = require('./libs/probe-server')
var config          = require("./config.json");

var REDIS_STR = "redis",
    FILE_STR  = "file";

if( config.input_mode != REDIS_STR && config.input_mode != FILE_STR)
    config.input_mode = FILE_STR;

//console.log( "configuration: " + JSON.stringify( config, null, "   " ) );

if( config.is_in_debug_mode !== true ){
    console.log = function( obj ){};
}

var dbconnector = new dbc( {
    connectString: 'mongodb://'+ config.database_server +':27017/mmt-bandwidth'
});


if( config.input_mode == REDIS_STR ){
    redis._createClient = redis.createClient;
    redis.createClient = function(){
        return redis._createClient(6379, config.redis_server, {});
    }

    probe.startListening(dbconnector, redis);
}
else{
    probe.startListeningAtFolder( dbconnector, config.data_folder);
}


function exit(){
    console.log("bye!\n");
    setTimeout( function(){
        process.exit(1);
    }, 1000 );
}

//clean up
function cleanup ( cb ){
    console.log( "\nCleaning up before exiting... ");
    if( dbconnector )
        dbconnector.flushCache( exit );
};


process.on('SIGINT',function(){
    try{
          cleanup();      
    }catch( err ){
        console.error( err );
        exit();
    }
});
