var dataAdaptor     = require('./libs/dataAdaptor');
var dbc             = require('./libs/mongo_connector');
var config          = require("./config.json");

var db = new dbc( {
    connectString: 'mongodb://'+ config.database_server +':27017/mmt-bandwidth'
});


var data = [
    //eth.ip.udp.netbios --> network
   [100,123,"eth1",1452778883.36,241,"99.178.376.241",    0,0,0,0, 5000000 ,0,0, 1000000 ,0,0,0,"ip_src","ip_dst", null, null ],
   //eth.ip.tcp.mysql  --> database
   [100,123,"eth1",1452778883.36,237,"99.178.354.237",    0,0,0,0, 5043900 ,0,0, 1000000 ,0,0,0,"ip_src","ip_dst", null, null ],
   //eth.ip.tcp.http.hotmail --> Mail 
   [100,123,"eth1",1452778883.36,152,"99.178.354.153.152",0,0,0,0, 4200400 ,0,0, 1000000 ,0,0,0,"ip_src","ip_dst", null, null ],
    //eth.ip.tcp.http.youtube
   [100,123,"eth1",1452778888.99,427,"99.178.376.153.427",0,0,0,0, 6000900 ,0,0, 1000000 ,0,0,0,"ip_src","ip_dst", null, null ],
    //eth.ip.tcp.http.facebook --> Social network
   [100,123,"eth1",1452778888.99,103,"99.178.376.153.103",0,0,0,0, 5890000 ,0,0, 1000000 ,0,0,0,"ip_src","ip_dst", null, null ],
    //eth.ip.tcp.http.torrentz --> P2P
   [100,123,"eth1",1452778888.99,365,"99.178.376.153.365",0,0,0,0, 1200000 ,0,0, 1000000 ,0,0,0,"ip_src","ip_dst", null, null ],
    //CLOUDFRONT --> CDN
   [100,123,"eth1",1452778888.99,593,"99.178.354.153.593",0,0,0,0, 2200000 ,0,0, 1000000 ,0,0,0,"ip_src","ip_dst", null, null ],
    //skype --> Conversational
   [100,123,"eth1",1452778888.99,317,"99.178.354.153.317",0,0,0,0, 9000000 ,0,0, 1000000 ,0,0,0,"ip_src","ip_dst", null, null ],
];

var data2 = [
    //eth.ip.tcp.http.torrentz --> P2P
   [100,123,"eth1",1452778888.99,365,"99.178.376.153.365",0,0,0,0, 1200000 ,0,0, 1000000 ,0,0,0,"ip_src","ip_dst", null, null ],
    //CLOUDFRONT --> CDN
   [100,123,"eth1",1452778888.99,593,"99.178.354.153.593",0,0,0,0, 2200000 ,0,0, 1000000 ,0,0,0,"ip_src","ip_dst", null, null ],
    //skype --> Conversational
   [100,123,"eth1",1452778888.99,317,"99.178.354.153.317",0,0,0,0, 9000000 ,0,0, 1000000 ,0,0,0,"ip_src","ip_dst", null, null ],
    //eth.ip.tcp.http.youtube
   [100,123,"eth1",1452778888.99,427,"99.178.376.153.427",0,0,0,0, 9000900 ,0,0, 1000000 ,0,0,0,"ip_src","ip_dst", null, null ],
    //eth.ip.tcp.http.facebook --> Social network
   [100,123,"eth1",1452778888.99,103,"99.178.376.153.103",0,0,0,0,   90000 ,0,0, 1000000 ,0,,00,"ip_src","ip_dst", null, null ],
    //eth.ip.tcp.http.torrentz --> P2P
   [100,123,"eth1",1452778888.99,365,"99.178.376.153.365",0,0,0,0,   50000 ,0,0, 1000000 ,0,0,0,"ip_src","ip_dst", null, null ],
    //CLOUDFRONT --> CDN
   [100,123,"eth1",1452778888.99,593,"99.178.354.153.593",0,0,0,0, 4200000 ,0,0, 1000000 ,0,0,0,"ip_src","ip_dst", null, null ],
    //skype --> Conversational
   [100,123,"eth1",1452778888.99,317,"99.178.354.153.317",0,0,0,0, 3000000 ,0,0, 1000000 ,0,0,0,"ip_src","ip_dst", null, null ],
];

var date = new Date();
console.log( "Current time is " + date.toLocaleTimeString() );
date.setMinutes( 0 );// at the begining of the current hour
var ip_prefix = "10.13.13.";

function insert_to_db(){
    var time = date.getTime() - 90*60*1000; //90 minute before
    for( var i=0; i<data.length; i++){
        var msg  = data[i];
        msg[ 3 ]   = time;                   //change timestamp
        msg[ 17 ]  = ip_prefix + ( 10 + i ); //change IP
        console.log( "ip: " + msg[ 17 ] + ", app: " + dataAdaptor.getAppName( msg[4] ) + ", category: " + dataAdaptor.getCategoryNameFromAppId( msg[4] ));
        db.addProtocolStats(msg, function (err, err_msg) { console.error( err ) });
    }

    console.log("");
    
    time = date.getTime() - 30*60*1000; //30 minute before
    for( var i=0; i<data2.length; i++){
        var msg  = data2[i];
        msg[ 3 ]   = time;                   //change timestamp
        msg[ 17 ]  = ip_prefix + ( 10 + i ); //change IP
        console.log( "ip: " + msg[ 17 ] + ", app: " + dataAdaptor.getAppName( msg[4] ) + ", category: " + dataAdaptor.getCategoryNameFromAppId( msg[4] ));
        db.addProtocolStats(msg, function (err, err_msg) { console.error( err ) });
    }
    
    db.flushCache( function(){ 
        console.log( "DONE!" ); 
        process.exit( 0 );
    });
}

//wait for db connection 
setTimeout(insert_to_db, 2000 );
