## Introduction
This program calculates preodically each hour bandwidths of network. Specifically:

- total bandwidth
- bandwidth of each user identifying by their MAC addresses
- bandwidth of each application
- bandwidth of each user of an application

Each bandwith has three values:

- total bandwidth
- input bandwidth
- output bandwidth

Unit of bandwidths is `bits per second`

## Configuration

The configuration is in `config.json` file:

- `database_server`: IP address of mongoDB
- `redis_server`: IP address of Redis server
- `data_folder` : Path to the folder containing output of mmt-probe
- `input_mode`: string, either `file` or `redis`
- `local_network`: Address IPs and its masks of local network. It is used to decide a packet is input or output:
    - `input`: if a packet having IP source is in the `local_network`.
    - `output`: if a packet having IP destination is in the `local_network`. All packets having IP source and destination are different with the one of the gateway will be omitted.
If this option is `null` then all packets will be traited and the iformation about directions may be not correct any more.
- `is_in_debug_mode`: boolean, set to true to print out debug information

## Installation

You need `nodejs` to run this program.

```bash
#install the dependencies
npm intall

#start the program
node app.js
```

If there are errors when `npm install` of `mongodb`, use `sudo apt-get install libkrb5-dev`