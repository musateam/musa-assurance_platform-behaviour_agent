var moment      = require('moment');
var dataAdaptor = require('./dataAdaptor.js');
var MongoClient = require('mongodb').MongoClient;
var config      = require('../config.json');

var MongoConnector = function (opts) {
    var self            = this;
    this.lastTimestamp  = 0;
    this.mdb            = null;
    var cache           = null;
    
    if (opts == undefined) opts = {};

    opts.connectString = opts.connectString || 'mongodb://localhost:27017/mmt-bandwidth';



    MongoClient.connect(opts.connectString, function (err, db) {
        if (err) throw err;
        self.mdb = db;
        //console.log("Connected to Database " + opts.connectString );
    });    


    
    /**
     * Stock a report to database
     * @param {[[Type]]} message [[Description]]
     */
    self.addProtocolStats = function (message) {
        if (self.mdb == null) return;

        var msg = dataAdaptor.formatReportItem(message);
        
        var ts = msg.time;
        if( msg.format !== 100 )
            return;
        
        ts = moment(msg.time).startOf(config.period).valueOf();

        //a new period
        if( self.lastTimestamp !== ts ){
            self.lastTimestamp = ts;
            self.flushCache();
        }
        var probe   = msg.probe;
        var source  = msg.source;
        var app     = msg.app;
        var user    = msg.ip_src;
        var input   = msg.dl_data;
        var output  = msg.ul_data;
        var total   = input + output;


        //total data in this period
        if( cache == undefined ) 
            cache = {total :  0, input: 0, output: 0, timestamp: ts, users: {}, apps: {}, probe: probe, source: source };
        cache.total  += total;
        cache.input  += input;
        cache.output += output;

        //data for this user
        if( cache.users[ user ] == undefined)
            cache.users[ user ] = {total :  0, input: 0, output: 0};
        cache.users[ user ].total  += total;
        cache.users[ user ].input  += input;
        cache.users[ user ].output += output;

        //data for this app
        if( cache.apps[ app ] == undefined)
            cache.apps[ app ] =  {total :  0, input: 0, output: 0, users: {}};
        cache.apps[ app ].total  += total;
        cache.apps[ app ].input  += input;
        cache.apps[ app ].output += output;

        //data for each user of a app
        if( cache.apps[ app ].users[ user ] == undefined)
            cache.apps[ app ].users[ user] = {total :  0, input: 0, output: 0};
        cache.apps[ app ].users[ user ].total  += total;
        cache.apps[ app ].users[ user ].input  += input;
        cache.apps[ app ].users[ user ].output += output;



        //data for the distination user
	/*
        user   = msg.ip_dest;
        input  = msg.dl_data;  //inverse from its source
        output = msg.ul_data;

        if( cache.users[ user ] == undefined)
            cache.users[ user ] = {total :  0, input: 0, output: 0};
        cache.users[ user ].total  += total;
        cache.users[ user ].input  += input;
        cache.users[ user ].output += output;


        //data for each user of an app
        if( cache.apps[ app ].users[ user ] == undefined)
            cache.apps[ app ].users[ user] = {total :  0, input: 0, output: 0};
        cache.apps[ app ].users[ user ].total  += total;
        cache.apps[ app ].users[ user ].input  += input;
        cache.apps[ app ].users[ user ].output += output;
	*/
    };

    var getRecord = function( data ){
        var period = 3600; //1 hour
        var obj    = {};
        obj.total  = Math.round( (data.total  * 8 )/ period );  //bit per second
        obj.input  = Math.round( (data.input  * 8 )/ period );  //bit per second
        obj.output = Math.round( (data.output * 8 )/ period );  //bit per second
        return obj;
    }

    self.flushCache = function (cb) {
	//console.log("==> flushing ");
        if ( cache ==  undefined ){
            if( cb ) cb();
            return;
        }
        
        var old_cache = cache;
        //reset cache
        cache = null;
        
        //insert data to db
        var ts      = old_cache.timestamp;
        var probe   = old_cache.probe;
        var source  = old_cache.source;

        var bandwidth = getRecord( old_cache );
        bandwidth.timestamp = ts;
        bandwidth.probe     = probe;
        bandwidth.source    = source;
        
        console.log("bandwidth at " + (new Date( ts )).toLocaleTimeString() + " is " + bandwidth.total + " bps");

        self.mdb.collection("total").insert(bandwidth, function (err, records) {
            if (err) console.error(err.stack);
        });

        //bandwidth of each user
        for( var user in old_cache.users ){
            var obj = getRecord( old_cache.users[ user ] )
            obj.ip        = user;
            obj.timestamp = ts;
            obj.probe     = probe;
            obj.source    = source;

            self.mdb.collection("users").insert(obj, function (err, records) {
                if (err) console.error(err.stack);
            });
        }

	var app_lst = [];
	var app_users_lst = [];

        //bandwidth of each app
        for( var app in old_cache.apps ){
            var data = old_cache.apps[ app ];
            app = parseInt( app );
            
            var category_name   = dataAdaptor.getCategoryNameFromAppId( app );
            var app_name        = dataAdaptor.getProtocolNameFromID( app );
//console.log("app: " + app_name );
            var obj             = getRecord( data )
            obj.app_id          = app;
            obj.app_name        = app_name;
            obj.timestamp       = ts;
            obj.category_name   = category_name;

            obj.probe     = probe;
            obj.source    = source;
	    
            app_lst.push( obj );
            
            
            //each user of an app
            data = data.users;
            for( var user in data ){
                var obj = getRecord( data[ user ] );
                obj.app_id          = app;
                obj.app_name        = app_name;
                obj.ip              = user;
                obj.timestamp       = ts;
                obj.category_name   = category_name;
                
                obj.probe     = probe;
                obj.source    = source;
		app_users_lst.push( obj );

                            }
        }
	self.mdb.collection("apps").insert(app_lst, function (err, records) {
                if (err) console.error(err.stack);
            });
	self.mdb.collection("app-users").insert(app_users_lst, function (err, records) {
                    if (err) console.error(err.stack);
                });


        if( cb ) cb();
    };
};

module.exports = MongoConnector;

